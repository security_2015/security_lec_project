#!/usr/bin/env python
from smtplib import SMTP 
from smtplib import SMTP_SSL 
from smtplib import SMTPException 
from email.MIMEMultipart import MIMEMultipart 
from email.MIMEText import MIMEText 
from email.MIMEImage import MIMEImage 
import sys
 
#Global varialbes
EMAIL_SUBJECT = "Declansare alarma" 
EMAIL_RECEIVERS = ['dragomirescu200@yahoo.co.uk'] 
EMAIL_SENDER = 'declansare_alarma@yahoo.com' 
TEXT_SUBTYPE = "plain"
YAHOO_SMTP = "smtp.mail.yahoo.com"
YAHOO_SMTP_PORT = 465
 
def listToStr(lst):
    """This method makes comma separated list item string"""
    return ','.join(lst)
 
def send_email(content, pswd, picture):
    """This method sends an email"""
    msg = MIMEMultipart()
    #msg = MIMEText(content, TEXT_SUBTYPE)
    msg["Subject"] = EMAIL_SUBJECT
    msg["From"] = EMAIL_SENDER
    msg["To"] = listToStr(EMAIL_RECEIVERS)
    body = MIMEMultipart('alternative')
    body.attach(MIMEText(content, TEXT_SUBTYPE ))
    #Attach the message
    msg.attach(body)
    #Attach a text file msg.attach(MIMEText(file("code4reference.txt").read())) Attach a picuture.
    msg.attach(MIMEImage(file(picture).read()))
     
    try:
      #Yahoo allows SMTP connection over SSL.
      smtpObj = SMTP_SSL(YAHOO_SMTP, YAHOO_SMTP_PORT)
      #If SMTP_SSL is used then ehlo and starttls call are not required.
      smtpObj.login(user=EMAIL_SENDER, password=pswd)
      smtpObj.sendmail(EMAIL_SENDER, EMAIL_RECEIVERS, msg.as_string())
      smtpObj.quit();
    except SMTPException as error:
      print "Error: unable to send email : {err}".format(err=error)
 
def main(pswd,picture):
    """This is a simple main() function which demonstrates sending of email using smtplib."""
    send_email("Alarma a fost declansata !", pswd, picture);
 
if __name__ == "__main__":
    """If this script is executed as stand alone then call main() function."""
    if len(sys.argv) == 3:
        main(sys.argv[1],sys.argv[2])
    else:
        print "Please provide password"
        sys.exit(0)
