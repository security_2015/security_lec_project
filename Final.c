#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int init_gpio()
{
	int fd;
	
	fd = open("/sys/class/gpio/gpio17/direction", O_WRONLY);
	if(fd == -1)
		return 0;
	write(fd, "in", 2);
	close(fd);
	
	return 1;
}

int main()
{
	ssize_t nrd;
	int fd;
	char buffer[10];

	if(init_gpio() == 0)
	{
		printf("Error in gpio initialization");
		return 1;
	}
	
	int stop=1;	
	while ( ((fd = open("/sys/class/gpio/gpio17/value", O_RDONLY)) != -1) && stop) {
		nrd = read(fd,buffer,1);
		buffer[nrd] = '\0';
		printf("%c\n", buffer[0]);
		usleep(1000000L);
		if(buffer[0]=='0')
		{
			printf("start\n");
			system("./Start_Alarm.py");
			stop=0;
		}
		close(fd);
	}
	
}
